package org.bitbucket.takahawk.java.strsearch.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import org.bitbucket.takahawk.java.strsearch.StringSearchLab;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = StringSearchLab.WIDTH;
		config.height = StringSearchLab.HEIGHT;
		config.resizable = false;
		config.title = "String search";
		new LwjglApplication(new StringSearchLab(), config);
	}
}
