package org.bitbucket.takahawk.java.strsearch.search;

import com.badlogic.gdx.utils.Array;
import net.dermetfan.utils.Pair;

/**
 * Created by takahawk on 22.03.16.
 */
public abstract class SubstringFinder {
    protected String string;
    protected String substring;


    public SubstringFinder(String string, String substring) {
        setString(string);
        setSubstring(substring);
    }

    public void setString(String string) {
        this.string = string;
    }

    public void setSubstring(String substring) {
        this.substring = substring;
    }

    public String getSubstring() {
        return substring;
    }

    public String getString() {
        return string;
    }

    abstract public int findNext(int begin);

    public int findFirst() {
        if (substring.equals(""))
            return -1;
        return findNext(0);
    }

    public Array<Pair<Integer, Integer>> findAll() {
        Array<Pair<Integer, Integer>> result = new Array<Pair<Integer, Integer>>();
        findAll(result);
        return result;
    }

    public void findAll(Array<Pair<Integer, Integer>> container) {
        container.clear();
        int i = findFirst();
        while (i != -1) {
            container.add(new Pair<Integer, Integer>(i, i + substring.length()));
            i = findNext(i + substring.length());
        }
    }
}
