package org.bitbucket.takahawk.java.strsearch.search;

/**
 * Created by takahawk on 24.03.16.
 */
public final class KnuthMorrisPrattFinder extends SubstringFinder {
    private int[] prefix;

    public KnuthMorrisPrattFinder(String string, String substring) {
        super(string, substring);
    }

    @Override
    public void setSubstring(String substring) {
        super.setSubstring(substring);
        prefix = getPrefixFunction(substring);
    }

    private int[] getPrefixFunction(String string) {
        int[] result = new int[string.length()];
        if (result.length == 0)
            return result;
        result[0] = 0;
        for (int i = 1; i < string.length(); i++) {
            int k = result[i - 1];
            while (k > 0 && string.charAt(i) != string.charAt(k)) {
                k = result[k - 1];
            }
            if (string.charAt(i) == string.charAt(k))
                k++;
            result[i] = k;
        }
        return result;
    }

    @Override
    public int findNext(int begin) {
        int k = 0;
        for (int i = begin; i < string.length(); i++) {
            while (k > 0 && substring.charAt(k) != string.charAt(i))
                k = prefix[k - 1];
            if (substring.charAt(k) == string.charAt(i))
                k++;
            if (k == substring.length()) {
                return i - substring.length() + 1;
            }
        }
        return -1;
    }
}
