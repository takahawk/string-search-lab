package org.bitbucket.takahawk.java.strsearch.search;

/**
 * Created by takahawk on 22.03.16.
 */
public final class NaiveSubstringFinder extends SubstringFinder {

    public NaiveSubstringFinder(String string, String substring) {
        super(string, substring);
    }

    @Override
    public int findNext(int begin) {
        int i = begin;
        while (i < string.length() - substring.length() + 1) {
            boolean match = true;
            for (int j = 0; j < substring.length(); j++) {
                if (substring.charAt(j) != string.charAt(i + j)) {
                    match = false;
                    break;
                }
            }
            if (match)
                return i;
            i++;
        }
        return -1;
    }
}
