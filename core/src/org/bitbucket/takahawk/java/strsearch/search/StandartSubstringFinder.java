package org.bitbucket.takahawk.java.strsearch.search;

/**
 * Created by takahawk on 24.03.16.
 */
public final class StandartSubstringFinder extends SubstringFinder {


    public StandartSubstringFinder(String string, String substring) {
        super(string, substring);
    }

    @Override
    public int findNext(int begin) {
        return string.indexOf(substring, begin);
    }
}
