package org.bitbucket.takahawk.java.strsearch.search;

/**
 * Created by sp213-08 on 31.03.2016.
 */
public class CopiedStandartFinder extends SubstringFinder {
    char[] stringAsArray;
    char[] substringAsArray;

    public CopiedStandartFinder(String string, String substring) {
        super(string, substring);
        stringAsArray = string.toCharArray();
        substringAsArray = string.toCharArray();
    }

    static int indexOf(char[] source, int sourceOffset, int sourceCount,
                       char[] target, int targetOffset, int targetCount,
                       int fromIndex) {
        if (fromIndex >= sourceCount) {
            return (targetCount == 0 ? sourceCount : -1);
        }
        if (fromIndex < 0) {
            fromIndex = 0;
        }
        if (targetCount == 0) {
            return fromIndex;
        }
        // aaaabaaaaab aaaaa
        char first = target[targetOffset];
        int max = sourceOffset + (sourceCount - targetCount);

        for (int i = sourceOffset + fromIndex; i <= max; i++) {
            /* Look for first character. */
            if (source[i] != first) {
                while (++i <= max && source[i] != first);
            }

            /* Found first character, now look at the rest of v2 */
            if (i <= max) {
                int j = i + 1;
                int end = j + targetCount - 1;
                for (int k = targetOffset + 1; j < end && source[j]
                        == target[k]; j++, k++);

                if (j == end) {
                    /* Found whole string. */
                    return i - sourceOffset;
                }
            }
        }
        return -1;
    }

    @Override
    public int findNext(int begin) {
        return indexOf(stringAsArray, 0, stringAsArray.length,
                substringAsArray, 0, substringAsArray.length, begin);
    }
}
