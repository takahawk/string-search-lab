package org.bitbucket.takahawk.java.strsearch.stages;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kotcrab.vis.ui.widget.VisWindow;
import net.dermetfan.gdx.scenes.scene2d.ui.FileChooser;
import net.dermetfan.gdx.scenes.scene2d.ui.TreeFileChooser;
import org.bitbucket.takahawk.java.strsearch.StringSearchLab;

import java.io.File;

/**
 * Created by takahawk on 19.03.16.
 */
public class FileStage extends Stage {
    private StringSearchLab context;
    private AssetManager assetManager;
    private Skin uiSkin;
    private FileHandle lastFile = null;
    public FileStage(Viewport viewport, StringSearchLab context) {
        super(viewport);
        this.context = context;
        assetManager = context.getAssetManager();
        assetManager.load("skins/uiskin/uiskin.json", Skin.class);
        assetManager.finishLoading();
        uiSkin = assetManager.get("skins/uiskin/uiskin.json");
        initStage();
    }

    public void initStage() {
        Window window = new VisWindow("Pick a file");
        window.setFillParent(true);
        TreeFileChooser fileChooser = new TreeFileChooser(uiSkin, new FileChooser.Listener() {
            @Override
            public void choose(FileHandle file) {
                lastFile = file;
                context.setMainStage();
            }

            @Override
            public void choose(Array<FileHandle> files) {
                lastFile = files.get(0);
                context.setMainStage();
            }

            @Override
            public void cancel() {
                context.setMainStage();
            }
        });

        for (File f : File.listRoots()) {
            fileChooser.add(new FileHandle(f));
        }
        fileChooser.getCell(fileChooser.getTreePane()).height(580).width(320);
        window.add(fileChooser);
        addActor(window);
    }

    public FileHandle getLastFile() {
        return lastFile;
    }

}
