package org.bitbucket.takahawk.java.strsearch.stages;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.*;
import net.dermetfan.utils.Pair;
import org.bitbucket.takahawk.java.strsearch.StringSearchLab;
import org.bitbucket.takahawk.java.strsearch.search.*;

/**
 * Created by takahawk on 20.03.16.
 */
public class MainStage extends Stage {
    private StringSearchLab context;
    private VisTextField word;
    private VisTextAreaWithHighlights textArea;
    private long time = 0;
    private SubstringFinder finder = new NaiveSubstringFinder("", "");
    private boolean searching = false;
    private Thread lastThread;

    static {
        VisUI.load();
    }

    public MainStage(Viewport viewport, StringSearchLab context) {
        super(viewport);
        this.context = context;
        initUI();
    }

    private void initUI() {
        Window window = new VisWindow("String Search");
        window.setFillParent(true);
        window.setMovable(false);

        Table table = new VisTable();
        table.setFillParent(true);
        VisTextArea fileTextField = new VisTextArea();
        fileTextField.setDisabled(true);
        TextButton browseFile = new VisTextButton("Browse...");
        browseFile.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                context.setFileStage();
            }
        });
        table.add(new VisLabel("File: ")).pad(15);
        table.add(fileTextField).pad(15).expandX();
        table.add(browseFile).pad(15);
        table.row();

        textArea = new VisTextAreaWithHighlights();
        table.add(textArea).colspan(3).pad(10).width(300).height(400);
        textArea.setCursorPosition(textArea.getText().length() / 2);
        table.row();

        VisTextButton nextButton = new VisTextButton("next");
        nextButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                int pos = textArea.getCursorPosition();
                for (Pair<Integer, Integer> hl : textArea.getHighlights()) {
                    if (hl.getValue() > pos) {
                        textArea.setSelection(hl.getKey(), hl.getValue());
                        textArea.setFirstLineShowing(textArea.getCursorLine() - textArea.getLinesShowing() / 2);
                        System.out.println("new selection " + hl.getKey() + ", " + hl.getValue());
                        // textArea.setCursorPosition(hl.getValue() + 1);
                        break;
                    }
                }
                MainStage.this.setKeyboardFocus(textArea);
            }
        });
        VisTextButton prevButton = new VisTextButton("prev");
        prevButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                int pos = textArea.getCursorPosition();
                for (int i = textArea.getHighlights().size - 1; i >= 0; i--) {
                    Pair<Integer, Integer> hl = textArea.getHighlights().get(i);
                    if (hl.getValue() < pos) {
                        textArea.setSelection(hl.getKey(), hl.getValue());

                        textArea.setFirstLineShowing(textArea.getCursorLine() - textArea.getLinesShowing() / 2);
                        System.out.println("new selection " + hl.getKey() + ", " + hl.getValue());
                        // textArea.setCursorPosition(hl.getValue() + 1);
                        break;
                    }
                }
                MainStage.this.setKeyboardFocus(textArea);
            }
        });
        table.add(); table.add(prevButton).expandX(); table.add(nextButton).expandX(); table.row();

        VisLabel resultsLabel = new VisLabel() {

            @Override
            public void act(float delta) {
                setText(
                        "Time: " + (searching ? System.nanoTime() - time : time) / 1000 + " microseconds" + "\n" +
                        "Occurences: " + textArea.getHighlights().size);
            }
        };
        table.add(resultsLabel).colspan(3).pad(10);
        table.row();

        word = new VisTextField();
        table.add(new VisLabel("Word: "));
        table.add(word).colspan(2);
        table.row();

        final VisSelectBox<String> algorithm = new VisSelectBox<String>();
        algorithm.setItems(new Array<String>(
                new String[] {
                        "naive",
                        "knuthMorrisPratt",
                        "javaStandard",
                        "copiedJavaStandard"
                }
        ));
        algorithm.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                switch (algorithm.getSelectedIndex()) {
                    case 0:
                        finder = new NaiveSubstringFinder(finder.getString(), finder.getSubstring());
                        break;
                    case 1:
                        finder = new KnuthMorrisPrattFinder(finder.getString(), finder.getSubstring());
                        break;
                    case 2:
                        finder = new StandartSubstringFinder(finder.getString(), finder.getSubstring());
                        break;
                    case 3:
                        finder = new CopiedStandartFinder(finder.getString(), finder.getSubstring());
                        break;
                    default:
                        throw new UnsupportedOperationException();
                }
                find();
            }
        });
        VisTextButton runButton = new VisTextButton("run");
        table.add(algorithm).colspan(2).pad(10);
        table.add(runButton);
        runButton.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                find();
            }
        });
        word.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                String substr = word.getText();
                if (substr.length() == 0) {
                    textArea.clearHighlights();
                    return;
                }
                finder.setSubstring(substr);
                find();
            }
        });
        textArea.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                finder.setString(textArea.getText());
                find();
            }
        });

        window.add(table);
        addActor(window);
    }

    public void find() {
        if (finder.getString() != null && finder.getSubstring() != null) {
                lastThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        searching = true;
                        Array<Pair<Integer, Integer>> newHighlights = new Array<Pair<Integer, Integer>>(textArea.getHighlights());
                        time = System.nanoTime();
                        finder.findAll(newHighlights);
                        textArea.setHighlights(newHighlights);
                        time = System.nanoTime() - time;
                        searching = false;
                    }
                });
                lastThread.start();
        }
    }

    public void setText(String text) {
        textArea.setText(text);
        finder.setString(textArea.getText());
    }

    @Override
    public boolean scrolled(int amount) {
        textArea.setFirstLineShowing(textArea.getFirstFineShowing() + amount);
        return true;
    }

    @Override
    public void dispose() {
        super.dispose();
        VisUI.dispose();
    }
}
