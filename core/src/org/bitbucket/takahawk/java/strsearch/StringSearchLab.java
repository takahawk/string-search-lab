package org.bitbucket.takahawk.java.strsearch;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import org.bitbucket.takahawk.java.strsearch.stages.FileStage;
import org.bitbucket.takahawk.java.strsearch.stages.MainStage;

public class StringSearchLab extends ApplicationAdapter {
	public static final int WIDTH = 360;
	public static final int HEIGHT = 640;
	public MainStage mainStage;
	public FileStage fileStage;
	public Stage currentStage;
	public Viewport viewport;
	public AssetManager assetManager = new AssetManager();

	@Override
	public void create () {
		viewport = new FitViewport(WIDTH, HEIGHT);
		mainStage = new MainStage(viewport, this);
		fileStage = new FileStage(viewport, this);
		currentStage = mainStage;
		Gdx.input.setInputProcessor(mainStage);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		currentStage.act();
		currentStage.draw();
	}

	public void setMainStage() {
		FileHandle lastFile = fileStage.getLastFile();
		if (lastFile != null) {
			String str = lastFile.readString();
			System.out.println(1);
			mainStage.setText(lastFile.readString());
			System.out.println(2);
		}
		currentStage = mainStage;
		Gdx.input.setInputProcessor(currentStage);
	}

	public void setFileStage() {
		currentStage = fileStage;
		Gdx.input.setInputProcessor(currentStage);
	}

	public AssetManager getAssetManager() {
		return assetManager;
	}
}
