# README #

Laboratory work on Java Technologies #2. Substring finding. Made with libGDX

### Running and build requires ###
* JDK6 (JAVA_HOME environment variable must be set to jdk directory)
* Android SDK for android

### How do I get set up? ###

1. `git clone`
2. `cd string-search-lab`
3. if needed set ANDROID_HOME variable point to your SDK 
*OR*
`touch local.properties` and fill it with sdk.dir=<PATH TO ANDROID SDK>  
4. 
* `./gradlew desktop:run` (`gradlew desktop:run` on Windows) run on desktop
* `./gradlew android:installDebug` (`gradlew android:installDebug` on Windows) android:run
* `./gradlew desktop:dist` (`gradlew desktop:dist` on Windows) build .jar in `desktop/build/libs`
* `./gradlew android:assembleRelease` (`gradlew android:assembleRelease` on Windows) build .apk in `android/build/outputs/apk`
5. You may use files in `testFiles/` directory to play around

Sure you may use your favourite IDE for running and building.

### Issues ###
* [VisTextArea](https://github.com/kotcrab/vis-editor/blob/master/ui/src/com/kotcrab/vis/ui/widget/VisTextArea.java) not optimized for big text data.